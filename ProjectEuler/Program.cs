﻿using System;
using System.Windows.Forms;

namespace ProjectEuler
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            ProblemBase problem = new Problem0001();
            string answer = problem.Run();

            Console.WriteLine("Answer: " + answer + '\n');
            Console.WriteLine("Press any key to quit. Press 'C' to copy answer to clipboard.");
            
            if(Console.ReadKey().Key == ConsoleKey.C)
            {
                Clipboard.SetText(answer);
            }
        }
    }
}
