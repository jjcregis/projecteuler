﻿using System.Collections.Generic;

namespace ProjectEuler
{
    class Problem0010 : ProblemBase
    {
        /*
         * Summation of primes
         * 
         * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
         * 
         * Find the sum of all the primes below two million.
         * 
         */

        public override string Compute()
        {
            long sum = 0;

            // Find the primes below two million using the Sieve of Eratosthenes
            List<long> primes = Primes.SieveOfEratosthenes(2000000);

            // Sum them
            foreach(long prime in primes)
            {
                sum += prime;
            }

            return sum.ToString();
        }

        
    }
}