﻿
namespace ProjectEuler
{
    class Problem0005 : ProblemBase
    {
        /*
         * Smallest multiple
         * 
         * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
         * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
         * 
         */

        public override string Compute()
        {
            long number = 20;

            // Don't check for 20, it will always be a factor.
            while (!(number % 11 == 0 &&
                    number % 12 == 0 &&
                    number % 13 == 0 &&
                    number % 14 == 0 &&
                    number % 15 == 0 &&
                    number % 16 == 0 &&
                    number % 17 == 0 &&
                    number % 18 == 0 &&
                    number % 19 == 0))
            {
                // Increase by the largest factor
                number += 20;
            }

            return number.ToString();
        }
    }
}
