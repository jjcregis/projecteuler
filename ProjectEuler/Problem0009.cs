﻿using System;

namespace ProjectEuler
{
    class Problem0009 : ProblemBase
    {
        /*
         * Special Pythagorean triplet
         * 
         * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which a^2 + b^2 = c^2
         * For example, 32 + 42 = 9 + 16 = 25 = 52.
         * 
         * There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
         * 
         */

        public override string Compute()
        {
            int sum = 0;

            for(int a= 1; a< 1000; a++)
            {
                for(int b = a; b < 1000; b++)
                {
                    for(int c = b; c < 1000; c++)
                    {
                        if(Math.Pow(a,2) + Math.Pow(b,2) == Math.Pow(c,2))
                        {
                            sum = a + b + c;

                            if(sum == 1000)
                            {
                                return (a * b * c).ToString();
                            }
                        }

                        if(sum > 1000)
                        {
                            break;
                        }
                    }
                }
            }
            return string.Empty;
        }
    }
}