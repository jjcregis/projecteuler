﻿
namespace ProjectEuler
{
    class Problem0014 : ProblemBase
    {
        /*
         * Longest Collatz sequence
         * 
         * The following iterative sequence is defined for the set of positive integers:
         * n → n/2 (n is even)
         * n → 3n + 1 (n is odd)
         * 
         * Using the rule above and starting with 13, we generate the following sequence:
         * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
         * 
         * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
         * Although it has not been proved yet (Collatz Problem), it is thought that all starting
         * numbers finish at 1.
         * 
         * Which starting number, under one million, produces the longest chain?
         * 
         * NOTE: Once the chain starts the terms are allowed to go above one million.
         * 
         */

        public override string Compute()
        {
            long longestChainStarter = 0;
            long longestChainLength = 0;

            long number = 0;
            long chainLength = 0;

            for (int i = 1; i < 1000000; i++ )
            {
                number = i;
                chainLength = 1;

                while(number != 1)
                {
                    number = Iterate(number);
                    chainLength++;
                }

                if(chainLength > longestChainLength)
                {
                    longestChainLength = chainLength;
                    longestChainStarter = i;
                }
            }

            return longestChainStarter.ToString();
        }

        private long Iterate(long number)
        {
            if(number % 2 == 0)
            {
                return number / 2;
            }
            else
            {
                return 3 * number + 1;
            }
        }
    }
}
