﻿
namespace ProjectEuler
{
    class Problem0004 : ProblemBase
    {
        /*
         * Largest palindrome product
         * 
         * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit
         * numbers is 9009 = 91 × 99. Find the largest palindrome made from the product of two 3-digit numbers.
         * 
         */

        public override string Compute()
        {
            int answer = 0;
            int product;

            for (int i = 999; i > 100; i--)
            {
                for (int j = i; j > 100; j--)
                {
                    product = i * j;

                    // Test if the answer is the largest palindrome
                    if(product > answer && IsPalindrome(product))
                    {
                        answer = product;
                    }
                }
            }

            return answer.ToString();
        }

        private bool IsPalindrome(int number)
        {
            int[] digits = new int[3];

            if (number % 100000 != number)
            {
                // Test if 6-digit number is a palindrome
                for(int i=0; i<6; i++)
                {
                    if(i<3)
                    {
                        // Store the first three digits
                        digits[i] = number % 10;
                    }
                    else
                    {
                        // Compare the last three digits against the first three
                        if(digits[5 - i] != number % 10)
                        {
                            return false;
                        }
                    }

                    // Divide by 10 to get the next digit
                    number /= 10;
                }
            }

            else
            {
                // Number is a 5-digit number
                for (int i = 0; i < 5; i++)
                {
                    if (i < 2)
                    {
                        // Store the first two digits. The third is skipped.
                        digits[i] = number % 10;
                    }
                    else if (i > 2)
                    {
                        // Compare the last two digits against the first two
                        if (digits[4 - i] != number % 10)
                        {
                            return false;
                        }
                    }

                    // Divide by 10 to get the next digit
                    number /= 10;
                }
            }

            return true;
        }
    }
}
