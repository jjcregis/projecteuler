﻿using System;

namespace ProjectEuler
{
    public abstract class ProblemBase
    {

        public string Run()
        {
            string answer;

            Console.WriteLine("Running " + GetType().Name + "...\n");

            DateTime beforeTime = DateTime.Now;
            answer = Compute();
            TimeSpan computationTime = DateTime.Now - beforeTime;

            Console.WriteLine("Computed in " + computationTime.TotalMilliseconds + " milliseconds.");
            return answer;
        }

        public virtual string Compute()
        {
            return string.Empty;
        }
    }
}
