﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler
{
    public static class Primes
    {
        public static List<long> SieveOfEratosthenes(long limit)
        {
            bool[] indexIsPrime = new bool[limit];
            long squareRootOfLimit = (long)Math.Sqrt(limit);

            List<long> primes = new List<long>();

            for (long i = 2; i < limit; i++)
            {
                indexIsPrime[i] = true;
            }

            for (long i = 2; i < squareRootOfLimit; i++)
            {
                if (indexIsPrime[i])
                {
                    for (long j = 2; i * j < limit; j++)
                    {
                        indexIsPrime[i * j] = false;
                    }
                }
            }

            // Return the list of prime numbers
            for (long i = 0; i < limit; i++)
            {
                if (indexIsPrime[i])
                {
                    primes.Add(i);
                }
            }
            return primes;
        }
    }
}
