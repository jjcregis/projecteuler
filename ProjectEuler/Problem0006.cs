﻿using System;

namespace ProjectEuler
{
    class Problem0006 : ProblemBase
    {
        /*
         * Sum square difference
         * 
         * The sum of the squares of the first ten natural numbers is: 1^2 + 2^2 + ... + 10^2 = 385
         * The square of the sum of the first ten natural numbers is: (1 + 2 + ... + 10)^2 = 552 = 3025
         * Hence the difference between the sum of the squares of the first ten natural numbers and the square
         * of the sum is 3025 − 385 = 2640.
         * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
         * 
         */

        public override string Compute()
        {
            long sumOfSquares = 0;
            long squareOfSums = 0;

            for (int i = 1; i <= 100; i++)
            {
                // Sum the square of each number
                sumOfSquares += (long)Math.Pow(i, 2);

                // Sum each number to square later
                squareOfSums += i;
            }

            // Square the sum of numbers 1-100
            squareOfSums = (long)Math.Pow(squareOfSums, 2);

            // Calculate the difference
            return (squareOfSums - sumOfSquares).ToString();
        }
    }
}
